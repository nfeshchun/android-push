package group.infotech.app

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Dinar on 02.11.2017.
 */
@Module
class AppModule(val ctx: Context) {

    @Provides
    @Singleton
    fun ctx() = ctx
}