package group.infotech.app

import android.app.Application
import group.infotech.android.push.NotificationService
import group.infotech.android.push.di.PushComponent
import javax.inject.Inject

/**
 * Created by Dinar on 02.11.2017.
 */
class App : Application() {

    @Inject lateinit var notif: NotificationService

    companion object {
        lateinit var injector: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        injector = DaggerAppComponent.builder().appModule(AppModule(this)).build()
        PushComponent.init(injector)
        injector.inject(this)
        notif.tokenManager.endpoint = { token ->
            url = "http://example.com/?token=$token"
            headers += "Auth" to "userId"
        }
    }
}