package group.infotech.app

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import group.infotech.android.push.NotificationInfo
import group.infotech.android.push.NotificationService
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : Activity() {

    @Inject lateinit var notif: NotificationService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.injector.inject(this)
        setContentView(R.layout.activity_main)
        btn_local.setOnClickListener {
            notif.addNotification(NotificationInfo(
                    id = 1,
                    title = text1.text.toString(),
                    text = text2.text.toString())
            )
        }

        val msg = """
            {
  "message":{
    "token" : %s,
    "notification" : {
      "title" : %s,
      "body" : %s
      }
   }
}
        """.trimIndent()
        btn_firebase.setOnClickListener {
            val token = notif.tokenManager.token
            when {
                token == null -> Toast.makeText(this, "NO TOKEN", Toast.LENGTH_SHORT).show()
                else -> {
                    //todo
                }
            }
        }
    }
}
