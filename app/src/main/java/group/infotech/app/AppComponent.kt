package group.infotech.app

import dagger.Component
import group.infotech.android.push.di.PushComponent
import group.infotech.android.push.di.PushModule
import javax.inject.Singleton

/**
 * Created by Dinar on 02.11.2017.
 */
@Component(
        modules = arrayOf(
                AppModule::class,
                PushModule::class
        )
)
@Singleton
interface AppComponent: PushComponent {
//    fun inject(mainActivity: TestActivity2)
//    fun inject(flowActivity: TestActivity)
    fun inject(app: App)

    fun inject(mainActivity: MainActivity)
}