#!/usr/bin/env bash

set -e
./gradlew clean build
./android-wait-for-emulator.sh 
./gradlew connectedCheck
