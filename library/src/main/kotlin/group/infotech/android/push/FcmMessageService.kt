package group.infotech.android.push

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

/**
 * Created by Dinar on 24.11.2017.
 */
internal class FcmMessageService : FirebaseMessagingService() {

    /**Если приложение открыто, то все типы оповещений приходят сюда
     * https://firebase.google.com/docs/cloud-messaging/android/receive
     */
    override fun onMessageReceived(message: RemoteMessage) {

    }

    override fun onDeletedMessages() {
        /**
         * Called when the FCM server deletes pending messages. This may be due to:
         * Too many messages stored on the FCM server. This can occur when an app's servers send a bunch of non-collapsible messages to FCM servers while the device is offline.
         * The device hasn't connected in a long time and the app server has recently (within the last 4 weeks) sent a message to the app on that device.
         * It is recommended that the app do a full sync with the app server after receiving this call. See here for more information.
         */
    }
}