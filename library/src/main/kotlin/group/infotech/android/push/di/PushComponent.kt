package group.infotech.android.push.di

import group.infotech.android.push.token.FcmTokenRefreshService
import group.infotech.android.push.token.FcmTokenSendService

/**
 * Created by Dinar on 24.11.2017.
 */
interface PushComponent {
    fun inject(service: FcmTokenSendService)
    fun inject(service: FcmTokenRefreshService)

    companion object {
        internal lateinit var injector: PushComponent
        fun init(injector: PushComponent) {
            this.injector = injector
        }
    }
}