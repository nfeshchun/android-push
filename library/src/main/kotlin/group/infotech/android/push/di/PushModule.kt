package group.infotech.android.push.di

import dagger.Binds
import dagger.Module
import group.infotech.android.push.NotificationService
import group.infotech.android.push.NotificationServiceDefault
import group.infotech.android.push.token.TokenManager
import group.infotech.android.push.token.TokenMangerDefault

/**
 * Created by Dinar on 24.11.2017.
 */
@Module
abstract class PushModule {
    @Binds abstract internal fun notif(impl: NotificationServiceDefault) : NotificationService
    @Binds abstract internal fun token(impl: TokenMangerDefault) : TokenManager
}