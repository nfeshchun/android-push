package group.infotech.android.push

import android.app.PendingIntent
import android.graphics.Bitmap

/**
 * Created by Dinar on 23.11.2017.
 */

class NotificationInfo(
        val id: Int,
        val title: String? = null,
        val titleRes: Int? = null,
        val text: String? = null,
        val textRes: Int? = null,
        val trayIcon: Int? = null,
        val iconRes: Int? = null,
        val iconBmp: Bitmap? = null,
        val vibrate: Boolean = false,
        val sound: Boolean = false,
        val led: Boolean = false,
        val onClick: PendingIntent? = null,
        val channel: String? = null)