package group.infotech.android.push.token

import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import group.infotech.android.push.NotificationService
import group.infotech.android.push.PushSettings
import group.infotech.android.push.di.PushComponent
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dinar on 23.11.2017.
 */
class FcmTokenRefreshService : FirebaseInstanceIdService() {

    @Inject lateinit var notif: NotificationService
    @Inject lateinit internal var settings: PushSettings

    override fun onCreate() {
        PushComponent.injector.inject(this)
        super.onCreate()
    }

    override fun onTokenRefresh() {
        val token = FirebaseInstanceId.getInstance().token
        val endpoint = notif.tokenManager.endpoint
        settings.currentToken = token
        when {
            token == null -> Timber.w("FCM token is null")
            endpoint == null -> {
                Timber.w("Endpoint for token not found")
                settings.tokenForSend = token
            }
            else -> FcmTokenSendService.start(baseContext, token, endpoint)
        }
    }
}