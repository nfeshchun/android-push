package group.infotech.android.push.token

/**
 * Created by Dinar on 24.11.2017.
 */
interface TokenManager {
    val token: String?
    var endpoint: TokenEndpointComposer?
}