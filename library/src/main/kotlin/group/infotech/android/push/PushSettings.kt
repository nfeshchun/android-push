package group.infotech.android.push

import android.content.Context
import jp.takuji31.koreference.KoreferenceModel
import jp.takuji31.koreference.nullableStringPreference
import jp.takuji31.koreference.stringPreference
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Dinar on 23.11.2017.
 */
@Singleton
internal class PushSettings @Inject constructor(
        ctx: Context
) : KoreferenceModel(ctx, "notifications-settings") {

    internal var tokenForSend by stringPreference()
    internal var currentToken by nullableStringPreference()
}