package group.infotech.android.push.token

import group.infotech.android.push.token.TokenRequest.Companion.POST
import okhttp3.MediaType
import okhttp3.Request
import okhttp3.RequestBody

/**
 * Created by Dinar on 23.11.2017.
 */
typealias TokenEndpointComposer = TokenRequest.(token: String) -> Unit

class TokenRequest(
        val method: Int = GET,
        var url: String = "",
        val postBody: String = "",
        val headers: MutableList<Pair<String, String>> = mutableListOf()) {

    companion object {
        const val GET = 0
        const val POST = 1
        private const val DELIMITER = "¬|^~"
        private const val HEADER_DELIMITER = "^|¬~"

        fun deserialize(string: String): TokenRequest {
            val data = string.split(DELIMITER)
            val actualHeaders = data[3]
                    .split(HEADER_DELIMITER)
                    .filter { it.isNotEmpty() }
            val headers = when {
                actualHeaders.isNotEmpty() -> actualHeaders.map {
                    it.split(": ").let { it[0] to it[1] }
                }
                else -> listOf()
            }
            return TokenRequest(
                    data[0].toInt(),
                    data[1],
                    data[2],
                    headers.toMutableList())
        }

        fun serialize(obj: TokenRequest): String {
            return obj.run {
                listOf(method, url, postBody,
                        headers.joinToString(HEADER_DELIMITER) { "${it.first}: ${it.second}" })
                        .joinToString(DELIMITER)
            }
        }
    }

    override fun toString(): String {
        val method = if (method == GET) "GET" else "POST"
        val headers = headers.joinToString("\n") { "${it.first}: ${it.second}" }
        return "METHOD: $method, URL: $url, HEADERS: $headers"
    }
}

internal fun TokenRequest.okHttpRequest(): Request.Builder {
    return Request.Builder().also {
        headers.forEach { (name, value) -> it.addHeader(name, value) }
        it.url(url)
        when(method) {
            POST -> {
                //todo add more mediaTypes if need
                val media = MediaType.parse("text/plain")
                val body = RequestBody.create(media, postBody)
                it.post(body)
            }
            else -> it.get()
        }
    }
}