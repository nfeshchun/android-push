package group.infotech.android.push.token

import android.content.Context
import group.infotech.android.push.PushSettings
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Dinar on 24.11.2017.
 */
@Singleton
internal class TokenMangerDefault @Inject constructor(
        val ctx: Context,
        val settings: PushSettings
): TokenManager {

    override var endpoint: TokenEndpointComposer? = null
        set(endpoint) {
            field = endpoint
            val token = settings.tokenForSend
            if (token.isNotEmpty() && endpoint != null) {
                FcmTokenSendService.start(ctx, token, endpoint)
            }
        }

    override val token: String? get() = settings.currentToken
}