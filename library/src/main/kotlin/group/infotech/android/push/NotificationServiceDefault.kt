package group.infotech.android.push

import android.app.Notification.DEFAULT_LIGHTS
import android.app.Notification.DEFAULT_SOUND
import android.app.NotificationManager
import android.content.Context
import android.graphics.BitmapFactory
import android.support.v4.app.NotificationCompat
import group.infotech.android.push.token.TokenManager
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Dinar on 23.11.2017.
 */
@Singleton
internal class NotificationServiceDefault @Inject constructor(
        val ctx: Context,
        val settings: PushSettings,
        override val tokenManager: TokenManager
) : NotificationService {

    private val manager = ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//    private val defaultChannel = ctx.packageManager.getApplicationLabel(ctx.applicationInfo).toString()

    override fun addNotification(notif: NotificationInfo) {
//        val channel = notif.channel ?: defaultChannel
        val notification = NotificationCompat.Builder(ctx).apply {
            mContentTitle = notif.title ?: notif.titleRes?.let(ctx::getString)
            mContentText = notif.text ?: notif.textRes?.let(ctx::getString)
            mStyle = NotificationCompat.BigTextStyle().apply {
                setBigContentTitle(notif.title)
                bigText(notif.text)
            }

            val trayIcon =  notif.trayIcon ?: R.drawable.ic_notification_tray_default
            setSmallIcon(trayIcon)
            val icon = notif.iconBmp ?: notif.iconRes?.let { BitmapFactory.decodeResource(ctx.resources, it) }
            icon?.let(this::setLargeIcon)

            val flags = listOf(
                    notif.vibrate to DEFAULT_LIGHTS,
                    notif.sound to DEFAULT_SOUND,
                    notif.led to DEFAULT_LIGHTS)
                    .filter { it.first }
                    .sumBy { it.second }
            setDefaults(flags)

            notif.onClick?.let(this::setContentIntent)

            setWhen(System.currentTimeMillis())
        }.build()

        manager.notify(notif.id, notification)
    }
}