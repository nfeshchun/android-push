package group.infotech.android.push.token

import android.content.Context
import android.os.Bundle
import com.google.android.gms.gcm.*
import group.infotech.android.push.PushSettings
import group.infotech.android.push.di.PushComponent
import okhttp3.OkHttpClient
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dinar on 23.11.2017.
 */
class FcmTokenSendService : GcmTaskService() {

    companion object {
        private const val REQUEST = "tokenHttpRequest"
        private const val TOKEN = "token"
        private val SERVICE = FcmTokenSendService::class.java
        private val TAG = SERVICE.name

        internal fun start(ctx: Context, token: String, request: TokenEndpointComposer) {
            val serialized = TokenRequest().run {
                request(this, token)
                TokenRequest.serialize(this)
            }
            val params = Bundle().apply {
                putString(REQUEST, serialized)
                putString(TOKEN, token)
            }

            val job = OneoffTask.Builder()
                    .setExtras(params)
                    .setRequiredNetwork(PeriodicTask.NETWORK_STATE_CONNECTED)
                    .setUpdateCurrent(true)
                    .setPersisted(true)
                    .setTag(TAG)
                    .setExecutionWindow(0, 1)
                    .setService(SERVICE)
                    .build()
            GcmNetworkManager.getInstance(ctx).schedule(job)
        }
    }

    @Inject lateinit internal var settings: PushSettings

    override fun onCreate() {
        super.onCreate()
        PushComponent.injector.inject(this)
    }

    override fun onRunTask(params: TaskParams): Int {
        val request = params.extras?.getString(REQUEST)?.let { TokenRequest.deserialize(it) }
        return when {
            request == null -> {
                Timber.e("Something went wrong. TokenRequest info not found.")
                GcmNetworkManager.RESULT_FAILURE
            }
            send(request) -> {
                settings.tokenForSend = ""
                GcmNetworkManager.RESULT_SUCCESS
            }
            else -> GcmNetworkManager.RESULT_RESCHEDULE
        }
    }

    private fun send(request: TokenRequest): Boolean {
        val okhttpRequest = request.okHttpRequest().build()
        Timber.i("Sending token: $request")
        return try {
            val response = OkHttpClient().newCall(okhttpRequest).execute()
            Timber.i("Sending token response: $response")
            response.code() == 200 //todo Add error handler to TokenEndpoint?
        } catch (e: Exception) {
            Timber.e(e,"Failed to send endpoint $request")
            false
        }
    }

    override fun onInitializeTasks() {
        // When your package is removed or updated, all of its network tasks are cleared by
        // the GcmNetworkManager. You can override this method to reschedule them in the case of
        // an updated package. This is not called when your application is first installed.
        //
        // This is called on your application's main thread.

        // TODO(developer): In a real app, this should be implemented to re-schedule important tasks.
    }
}