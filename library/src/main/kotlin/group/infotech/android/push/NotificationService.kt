package group.infotech.android.push

import group.infotech.android.push.token.TokenManager

/**
 * Created by Dinar on 23.11.2017.
 */
interface NotificationService {

    val tokenManager: TokenManager

    fun addNotification(notif: NotificationInfo)
}