android-push
=======

## Подключение:


Добавить Firebase google-services.json в ваше апк

#### root build.gradle
```groovy
buildscript {
    
    dependencies {
        classpath "classpath 'com.google.gms:google-services:3.1.0'"
    }
    
}

repositories {
        maven {
            url "http://repo.infotech.team/artifactory/android/"

            if (project.hasProperty("INFOTECH_USER")) {
                credentials {
                    username INFOTECH_USER
                    password INFOTECH_PASS
                }
            }
        }
    }

dependencies {
    
}
//эту строку добавлять в САМОМ КОНЦЕ
apply plugin: 'com.google.gms.google-services'
```

####app build.gradle

```groovy
dependencies {
    compile "group.infotech:android-push:$latest"    
}    
```

## Начало работы :
Коммуникация с модулем осуществляется через `Dagger 2`:

```kotlin
//Рут модуль приложения должен предоставлять контекст
@Module
class AppModule(val ctx: Context) {
    @Provides @Singleton fun ctx(): Context = ctx 
}
//Для связи графов зависимостей библиотеки и приложения 
//рут компонент должен наследоваться(зачем смотрите дальше) от компонента библиотеки  
//и добавлять его модуль.  
@Component(modules = arrayOf(
    PushModule::class
    //остальные модули
))
interface AppComponent : PushComponent {
    //ваши inject методы
}
//Примерно так должен выглядит App класс приложения 
class App : Application() {

    companion object {
        lateinit var injector: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        injector = DaggerAppComponent.builder().appModule(AppModule(this)).build()
        PushComponent.init(injector)
    }
}
```

